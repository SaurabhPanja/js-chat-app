//fetch urls
let channelsUrl = "http://localhost:5000/channels",
  messageUrl = "http://localhost:5000/messages",
  currentChannelId,
  username;

//app initialization
window.onload = () => {
  $("#get-user-modal").modal("show");
  getUserName();
  getAllChannel();
};

const getUserName = () => {
  let container = document.querySelector(".ui.relaxed.grid.container");
  container.style.display = "none";
  let usernameInput = document.querySelector("#get-username");
  let usernameInputButton = document.querySelector(
    ".ui.positive.right.labeled.icon.button"
  );
  usernameInputButton.addEventListener("click", () => {
    event.preventDefault();
    username = usernameInput.value;
    container.style.display = "block";
    if (!username) username = "aham bramhasmi";
  });
};

//channels functions

const getAllMsgFromChannel = channel_id => {
  currentChannelId = channel_id;
  fetch(`${messageUrl}?channel_id=${channel_id}`)
    .then(res => res.json())
    .then(allChannelMsgs =>
      showAllMsgFromChannel(allChannelMsgs.resources, channel_id)
    );
};

const showAllMsgFromChannel = (allChannelMsgs, channelId) => {
  fetch(`${channelsUrl}/?id=${channelId}`)
    .then(res => res.json())
    .then(result => result.resources[0].name)
    .then(channelName => displayChannelName(channelName));

  let displayMessage = document.querySelector("#display-msg");
  displayMessage.innerHTML = "";
  let appendToDisplayMessage = "";
  allChannelMsgs.forEach(channelMsg => {
    appendToDisplayMessage += `
    <div class="ui cards">
      <div class="card">
        <div class="content">
        <div class="header">
        ${channelMsg.username}
        </div>
        <div class="description">
        ${channelMsg.text}
        </div>
      </div>
    </div>
  </div>`;
  });
  displayMessage.innerHTML += appendToDisplayMessage;
};

const displayChannelName = channelName => {
  let channelNameH1 = document.querySelector("#channel-name");
  channelNameH1.innerText = channelName;
};

//get request
const getAllChannel = () =>
  fetch(channelsUrl)
    .then(res => res.json())
    .then(result => {
      let channelArray = result.resources;
      return channelArray;
    })
    .then(channels => listAllChannels(channels))
    .then(() => onClickBgChange());

//background change channel item on click
const onClickBgChange = () => {
  let allChannelItems = document.querySelectorAll(".item");
  allChannelItems.forEach(channelItem => {
    channelItem.addEventListener("click", () => {
      allChannelItems.forEach(channelItem => {
        channelItem.classList.remove("active");
      });
      channelItem.classList.add("active");
    });
  });
};


const newChannelButton = () => {
  let newChannelName = document.querySelector("#new-channel-name");
  event.preventDefault();
  addChannelToDB(newChannelName.value);
  getAllChannel();
  newChannelName.value = "";
};

const listAllChannels = channels => {
  let allChannels = document.querySelector(".all-channels");
  let appendChannels = "";
  channels.forEach(channel => {
    appendChannels += `<div class="item" onclick="getAllMsgFromChannel(${channel.id})">${channel.name}</div>`;
  });
  allChannels.innerHTML = appendChannels;
};

//post request

const addChannelToDB = newChannelName => {
  fetch(channelsUrl, {
    method: "post",
    body: JSON.stringify({
      name: newChannelName
    })
  })
    .then(() => console.log("Done"))
    .then(getAllChannel())
    .catch(err => {
      console.log("Error" + err);
    });
};

//Messages functions
//send messages

const sendMsg = () => {
  event.preventDefault();
  let newMsg = document.querySelector("#new-msg");
  addMsgToDB(username, newMsg.value, currentChannelId);
  newMsg.value = "";
};

const addMsgToDB = (username, text, currentChannelId) => {
  fetch(messageUrl, {
    method: "POST",
    body: JSON.stringify({
      username: username,
      text: text,
      channel_id: currentChannelId
    })
  })
    .then(res => res.json())
    .then(result => broadcastMessage(result));
};

const broadcastMessage = data => {
  fetch("http://localhost:5000/broadcast", {
    method: "post",
    body: JSON.stringify(data)
  });
};

//pusherMsg
const appendMessageToChannel = data => {
  if (currentChannelId === data.channel_id) {
    let displayMessage = document.querySelector("#display-msg");
    displayMessage.innerHTML += `<div id="scroll" class="ui cards">
    <div class="card">
      <div class="content">
      <div class="header">
      ${data.username}
      </div>
      <div class="description">
      ${data.text}
      </div>
    </div>
  </div>
</div>`;
    let scrollToNewMsg = document.querySelector("#scroll");
    scrollToNewMsg.scrollIntoView();
  }
};
