// console.log('Hello');

// let username = prompt("Enter username");

// if(!username)
//     username = "aham bramhasmi";

// let username = "Dummy";

let messageUrl = 'http://localhost:5000/messages/';

const sendMsg = () =>{
    // console.log('Added message to db');
    event.preventDefault();
    let newMsg = document.querySelector('#new-msg');
    console.log(newMsg.value, username);
    newMsg.value = "";
}

// post messages to db

const addMsgToDB = () =>{
    fetch(messageUrl,{
        method : 'POST',
        body : JSON.stringify({
            username    : 'MS Dhoni',
            text       : 'I will never announce my retirement',
            channel_id : 2
        })
    })
    .then(()=>console.log('Done'));
}

const getAllMsgFromChannelId = (channel_id) =>{
    fetch(`${messageUrl}?channel_id=${channel_id}`)
        .then(res=>res.json())
            .then(result=>{
                // console.log(result.resources.username,result.resources.text);
                let messageArr = result.resources;
                messageArr.forEach(message => {
                    console.log(message.text, message.username);
                });
            })
}

addMsgToDB();
getAllMsgFromChannelId(1);

fetch('http://localhost:5000/broadcast',{
    method : 'post'
}).then(res=>console.log(res));